<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class qrcodes extends Model
{
    use HasFactory;

    protected $fillable=[
        'id_escursione',
        'hash',
        'validato',
    ];

    public function escursione(){
        return $this->hasOne(escursione::class,'id');
    }
}
