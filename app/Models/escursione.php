<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class escursione extends Model
{
    use HasFactory;

    protected $fillable=[
      
      'adulti',
      'child',
      'infant',
      'qrcode',  
        
    ];

    public function qrcode(){
        return $this->hasMany(qrcode::class);
    }
}
