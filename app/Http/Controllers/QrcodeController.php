<?php

namespace App\Http\Controllers;

use App\Models\qrcodes;
use App\Models\escursione;
use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class QrcodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $escursione = escursione::create([
            'adulti' => 2,
            'child' => 0,
            'infant' => 0,
            'qrcode' => 1,
        ]);
        /* $key = bin2hex(random_bytes(32)); */
        $qrcode = qrcodes::create([
            'id_escursione' => $escursione->id,
            'hash' => bin2hex(random_bytes(32)),
        ]);

        $qrcodeGen = QrCode::format('png')->size(200)->generate("http://127.0.0.1/hash=" . $qrcode->hash . "&idesc=" . $qrcode->id_escursione, '../public/storage/qrcodes/qr_' . $qrcode->id . '_' . $qrcode->id_escursione . '.png');


        return redirect()->back();
    }

    public function verificaqrcode(Request $request)
    {
        
        $idescursione = $request->idescursione;
        $keysicurezza = $request->keysicurezza;

        //verifica validità dati

        //elaborazione lettura DB
        //risultati possibili
        // valido non utilizzato -> 1
        // valido già utilizzato -> 2
        // non valido ANOMALIA   -> 3

        $tiposervizio = "Funivia Adulto"; //<- descrizione risultate dalla ricerca su db nella tabella servizi
        $nome = "Antonio";
        $cognome = "Russotti";
        $pax = "1";
        $data = "27/04/2023";
        $prezzo = 50.00;

        $jsonresult = [
            "verificastato"=> 1,
            "idescursione" => $idescursione,
            "keysicurezza" => $keysicurezza,
            "tiposervizio" => $tiposervizio,
            "nome"=>$nome,
            "cognome"=>$cognome,
            "pax"=>$pax,
            "data"=>$data,
            "prezzo"=>$prezzo
        ];

        return response()->json($jsonresult);
    }


    public function confermavoucher($id)
    {

        $qrcode = qrcodes::where('id', $id)->update(['validato' => 1]);

        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\qrcode  $qrcode
     * @return \Illuminate\Http\Response
     */
    public function show(qrcode $qrcode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\qrcode  $qrcode
     * @return \Illuminate\Http\Response
     */
    public function edit(qrcode $qrcode)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\qrcode  $qrcode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, qrcode $qrcode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\qrcode  $qrcode
     * @return \Illuminate\Http\Response
     */
    public function destroy(qrcode $qrcode)
    {
        //
    }
}
