<?php

namespace App\Http\Controllers;

use App\Models\escursione;
use App\Models\qrcodes;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function home(){
        $text=array();
        $escursione=escursione::all();
        /* dd($escursione->qrcode); */
        $qrcode=qrcodes::with('escursione')->get();
        
        foreach ($qrcode as $key) {
            
            $idc=$key->escursione->id;
            $idhash=$key->hash;
            $text[]="https://127.0.0.1/ide=".$idc."&idhash=".$idhash;
        }
        
        return view('welcome')->with(compact('qrcode','text'));
    }
}
