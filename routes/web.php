<?php

use App\Http\Controllers\PublicController;
use App\Http\Controllers\QrcodeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */

Route::get('/',[PublicController::class,'home'])->name('home');
Route::get('/qrcode',[QrcodeController::class,'create'])->name('qrcodegen');
Route::get('/conferma{id}',[QrcodeController::class,'confermavoucher'])->name('confermavoucher');
Route::any('/verificadati',[QrcodeController::class,'verificaqrcode'])->name('verificadati');
