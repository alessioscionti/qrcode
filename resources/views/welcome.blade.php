<x-layout>
    <div class="container-fluid border rounded shadow-lg bg-secondary" style="max-width:820px;width:auto;overflow-x:hidden">
        <div class="row justify-content-center mt-2">
            <div class="col-12 col-xs-4 col-sm-4 text-center">
                <span style="color:white" class="fs-1 text-nowrap m-2">Lettura QrCode</span>
                <div class="container-fluid dfm rounded shadow border bg-light mt-2" style="height:250px;width:250px"></div>
            </div>
            <div class="col-12 col-xs-7 col-sm-7 text-center ps-1">
                <span style="color:white" class="fs-1 m-2 title-biglietto">Dati Biglietto</span>
                <div class="container-fluid mt-2 dati-biglietto">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="container-fluid">
                                <div class="row justify-content-center">
                                    <div class="col-12 col-xs-6 col-sm-6 text-end">
                                        <label for="prenotazione" class="text-uppercase fs-5 fw-bold prenotazione">Prenotazione: </label>
                                    </div>
                                    <div class="col-12 col-xs-6 col-sm-6 text-start">
                                        <input type="text" name="prenotazione" id="prenotazione" class="border rounded shadow select-input">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 mt-2">
                            <div class="container-fluid">
                                <div class="row justify-content-center">
                                    <div class="col-12 col-xs-6 col-sm-6 text-end">
                                        <label for="prenotazione" class="text-uppercase fs-5 fw-bold tiposerv">TIPO SERVIZIO: </label>
                                    </div>
                                    <div class="col-12 col-xs-6 col-sm-6 text-start">
                                        <select name="servizio" id="servizio" class="border rounded shadow text-center select-input" id="" disabled>
                                            <option value="">SELEZIONA</option>
                                            <option value="">FUNIVIA ADULTO</option>
                                            <option value="">FUNIVIA BAMBINO</option>
                                            <option value="">COMPLETO ADULTO</option>
                                            <option value="">COMPLETO BAMBINO</option>
                                        </select>
                                        <input type="hidden" class="border rounded shadow">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 mt-2">
                            <div class="container-fluid">
                                <div class="row justify-content-center">
                                    <div class="col-12 col-xs-6 col-sm-6 text-end">
                                        <label for="prenotazione" class="text-uppercase fs-5 fw-bold nome">NOME: </label>
                                    </div>
                                    <div class="col-12 col-xs-6 col-sm-6 text-start">
                                        <input type="text" name="nome" id="nome" class="border rounded shadow select-input">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 mt-2">
                            <div class="container-fluid">
                                <div class="row justify-content-center">
                                    <div class="col-12 col-xs-6 col-sm-6 text-end">
                                        <label for="prenotazione" class="text-uppercase fs-5 fw-bold cognome">cognome: </label>
                                    </div>
                                    <div class="col-12 col-xs-6 col-sm-6 text-start">
                                        <input type="text" name="cognome" id="cognome" class="border rounded shadow select-input">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 mt-2">
                            <div class="container-fluid">
                                <div class="row justify-content-center">
                                    <div class="col-12 col-xs-6 col-sm-6 text-end">
                                        <label for="prenotazione" class="text-uppercase fs-5 fw-bold pax">pax: </label>
                                    </div>
                                    <div class="col-12 col-xs-6 col-sm-6 text-start">
                                        <input type="text" name="pax" id="pax" class="border rounded shadow select-input">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 mt-2">
                            <div class="container-fluid">
                                <div class="row justify-content-center">
                                    <div class="col-12 col-xs-6 col-sm-6 text-end">
                                        <label for="prenotazione" class="text-uppercase fs-5 fw-bold data">data: </label>
                                    </div>
                                    <div class="col-12 col-xs-6 col-sm-6 text-start">
                                        <input type="text" name="data" id="data" class="border rounded shadow select-input">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 mt-2">
                            <div class="container-fluid">
                                <div class="row justify-content-center">
                                    <div class="col-12 col-xs-6 col-sm-6 text-end">
                                        <label for="prenotazione" class="text-uppercase fs-5 fw-bold prezzo">prezzo: </label>
                                    </div>
                                    <div class="col-12 col-xs-6 col-sm-6 text-start">
                                        <input type="text" name="prezzo" id="prezzo" class="border rounded shadow select-input">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center bg-info rounded shadow-lg ms-1 me-1 mt-3">
            <div class="col-12 text-center">
                <h1>messaggio</h1>
            </div>
        </div>
        {{-- INIZIO BOTTONI --}}
        <div class="container-fluid mt-1">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="container-fluid">
                        <div class="row justify-content-start">
                            <div class="col-12 col-xs-1 col-sm-1 ps-0 me-4 text-start">
                                <button class="btn btn-success btn-lg">CONFERMA</button>
                            </div>
                            <div class="col-12 col-xs-8 col-sm-8 ms-4 me-n5 pe-0 text-center">
                                <button class="btn btn-warning btn-lg btn-modifica">MODIFICA</button>
                            </div>
                            <div class="col-2">
                                <div class="container-fluid">
                                    <div class="row justify-content-center">
                                        <div class="col-12 ms-n5 ps-0">
                                            <button class="btn btn-danger btn-lg2 text-nowrap">INDIETRO</button>
                                        </div>
                                        <div class="col-12 ms-n5 ps-0">
                                            <button class="btn btn-danger btn-lg2 text-nowrap">RIESEGUI SCAN</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <span>Powered by Kemedia</span>
    </div>
{{-- SCRIPT QRCODE --}}
<script src="https://code.jquery.com/jquery-3.6.4.min.js" integrity="sha256-oP6HI9z1XaZNBrJURtCoUT5SUnxFr8s3BzRl+cbzUq8=" crossorigin="anonymous"></script>
<script src="{{asset('js/html5-qrcode.min.js')}}" defer></script>

<script>

    $(document).ready(function () {
        $("#bottone001").click(function () {
            console.log("bottone");
        })
    })

    function onScanSuccess(decodedText, decodedResult) {
            // Handle on success condition with the decoded text or result.
            console.log(`Scan result: ${decodedText}`, decodedResult);
            
            var resultContainer = document.getElementById('qr-reader-results');
            resultContainer.innerHTML += `<div>[ ${decodedText} ]</div>`;
            
            // Optional: To close the QR code scannign after the result is found
            html5QrcodeScanner.clear();

        }

        var html5QrcodeScanner = new Html5QrcodeScanner("reader", { fps: 10, qrbox: 250 });

        html5QrcodeScanner.render(onScanSuccess);

</script>
</x-layout>