<x-layout>
    <?php 
          
        foreach ($text as $key) {
            $keys=explode('=',$key);
            $k=explode('&',$keys[1]);
            $idesc=$k[0];
            $hash=$keys[2];
            
        }
    ?>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                <div id="id"></div>
            </div>
        </div>
    </div>
    <div class="container-fluid mt-5">
        <div class="row justify-content-center">
            <div class="col-12 text-center">
                <h1 style="color:black">Prenotazioni QrCode</h1>
                
                    <div class="container-fluid mt-5">
                        <div class="row justify-content-center">
                            @foreach ($qrcode as $item)
                            <div class="col-12 col-xs-12 col-sm-12 col-lg-4 col-xl-4 col-xxl-4 text-center border rounded p-3 m-1 shadow-lg">
                            @if ($item->validato==0)
                            <div class="container">
                                <h1>Da validare</h1>
                                @if (hash_equals($item->hash, $key))
                                    @if ($item->hash==$hash)
                                    <input type="hidden" id="codiceqr" value="{{$text}}">
                                    <h2>codice valido id: {{$item->id}}</h2>
                                    <p>Adulti: <span>{{$item->escursione->adulti}}</span></p>
                                    <p>Bambini: <span>{{$item->escursione->child}}</span></p>
                                    <p>Infanti: <span>{{$item->escursione->infant}}</span></p>
                                    <a class="btn btn-success" href="{{route('confermavoucher',$item->id)}}">Conferma Voucher</a>
                                    @endif
                                @else
                                <h2>codice da validare id: {{$item->id}}</h2>
                                <img class="img-responsive" src="{{asset('storage/qrcodes/qr_'.$item->id.'_'.$item->id_escursione.'.png')}}" alt="">
                                <p>Adulti: <span>{{$item->escursione->adulti}}</span></p>
                                <p>Bambini: <span>{{$item->escursione->child}}</span></p>
                                <p>Infanti: <span>{{$item->escursione->infant}}</span></p>
                                @endif
                            </div>
                            @else
                            <div class="container">
                                <h1>Validati</h1>
                                <h2>codici gia validati id: {{$item->id}}</h2>
                                <p>Adulti: <span>{{$item->escursione->adulti}}</span></p>
                                <p>Bambini: <span>{{$item->escursione->child}}</span></p>
                                <p>Infanti: <span>{{$item->escursione->infant}}</span></p>
                            </div>
                                @endif
                            </div>
                            @endforeach
                        </div>
                    </div>
            </div>
        </div>
    </div>
    
 <script src="https://raw.githubusercontent.com/mebjas/html5-qrcode/master/minified/html5-qrcode.min.js"></script>
 <script>
    Html5Qrcode.getCameras().then(cameras => {
      /**
        * devices would be an array of objects of type:
        * { id: "id", label: "label" }
       */
       if (devices && devices.length) {
           var cameraId = devices[0].id;
            // .. use this to start scanning.
        }
   }).catch(err => {
        // handle err   
});
 </script>
</x-layout>
