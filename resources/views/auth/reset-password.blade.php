<style>
    @import "compass/css3";
    @import url('https://fonts.googleapis.com/css2?family=Neonderthaw&display=swap');
     h1 {
         color: rgb(85, 8, 8);
         text-align: center;
         font-family: 'Neonderthaw', cursive;
         font-size: 50px;
    }
     .login-form {
         width: 350px;
         padding: 40px 30px;
         background: #fefeffc9;
         margin: auto;
         position: absolute;
         left: 0;
         right: 0;
         top: 30%;
         border-radius: 10px;
         
    }
     .form-group {
         position: relative;
         margin-bottom: 15px;
         filter: blur(0px);
         
    }
     .form-control {
         width: 100%;
         height: 50px;
         border: none;
         padding: 5px 7px 5px 15px;
         background: #fff;
         color: #666;
         border: 2px solid #ddd;
         filter: blur(0px);
    
    }
     .form-control:focus, .form-control:focus + .fa {
         border-color: #10ce88;
         color: #10ce88;
    }
     .form-group .fa {
         position: absolute;
         right: 15px;
         top: 17px;
         color: #999;
    }
     .log-status.wrong-entry .form-control, .wrong-entry .form-control + .fa {
         border-color: #ed1c24;
         color: #ed1c24;
    }
     .log-btn {
         background: #0ac986;
         dispaly: inline-block;
         width: 100%;
         font-size: 16px;
         height: 50px;
         color: #fff;
         text-decoration: none;
         border: none;
    }
     .link {
         text-decoration: none;
         color: #c6c6c6;
         float: right;
         font-size: 12px;
         margin-bottom: 15px;
    }
     .link:hover {
         text-decoration: underline;
         color: #8c918f;
    }
     .alert {
         font-size: 22px;
         color: #f00;
         float: left;
    }
    .form-group .far {
        position: absolute;
        right: 15px;
        top: 17px;
        color: #999;
    }
</style>
<x-layout>
<main>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 text-center" style="width: 50%">
                <form method="POST" action="{{ route('password.update') }}">
                    @csrf
                    <input type="hidden" name="token" value="{{ $request->route('token') }}">
                    <div class="login-form shadow">
                        <h1 class="h1form">Enoteca Dell'Etna</h1>
                        <div class="form-group ">
                            {{-- <label for="email" value="{{ __('Email') }}">{{ __('Email') }}</label> --}}
                            <input id="email" class="form-control" type="email" name="email" :value="old('email', $request->email)" placeholder="email" required autofocus>
                            <i class="fa fa-user"></i>
                        </div>
                        <div class="form-group ">
                            {{-- <label for="password" value="{{ __('Password') }}">{{ __('Password') }}</label> --}}
                            <input id="password" class="form-control" placeholder="Nuova password" type="password" name="password" required autocomplete="new-password" />
                            <input type="checkbox" class="check far fa-eye" style="appearance: none;">
                        </div>
                        <div class="form-group ">
                            {{-- <label for="password_confirmation" value="{{ __('Confirm Password') }}">{{ __('Confirm Password') }}</label> --}}
                            <input id="password_confirmation" placeholder="Conferma nuova password" class="form-control" type="password" name="password_confirmation" required autocomplete="new-password" />
                            <input type="checkbox" class="check far fa-eye" style="appearance: none;">
                        </div>
                        <button class="btn btn-success" type="submit">Salva Nuova Password</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
</x-layout>
